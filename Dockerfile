# Intermediary image used to compile the requirements
FROM python:3.6-slim AS compile-image

RUN python -m pip install --upgrade pip

# Create and activate the virtual environment
RUN pip install virtualenv
RUN python -m virtualenv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

# Install requirements
COPY requirements.txt .
RUN pip install -r requirements.txt

# Install the pre-built application
COPY dist/*.whl .
RUN pip install *.whl


# The final image
FROM python:3.6-slim AS build-image

# Create a "worker" user
RUN adduser --home /home/worker --gid 0 --gecos "Runtime user for gunicorn" --disabled-password --disabled-login worker
USER worker

# Copy and activate the virtual environment
COPY --from=compile-image --chown=worker /opt/venv /home/worker/venv
ENV PATH="/home/worker/venv/bin:$PATH"

# Initialize the database
RUN python -c "from flaskblog import core; core.init_db()" && \
    chmod -R g=u /home/worker /tmp/database.db && \
    ls -l /home/worker

EXPOSE 8080

ENTRYPOINT ["python", "-m", "gunicorn", "--worker-tmp-dir", "/dev/shm", "--bind=0.0.0.0:8080", "--workers=2", "flaskblog.core:application"]
