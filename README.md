# showcase-python-flask

[![pipeline status](https://gitlab.com/j2c-bce/showcase-python-flask/badges/main/pipeline.svg)](https://gitlab.com/j2c-bce/showcase-python-flask/-/pipelines)
[![coverage report](https://gitlab.com/j2c-bce/showcase-python-flask/badges/main/coverage.svg)](https://gitlab.com/j2c-bce/showcase-python-flask/-/tree/main/tests)

Showcase for a Flask based simple web application, modeled after the tutorial in
https://www.digitalocean.com/community/tutorials/how-to-make-a-web-application-using-flask-in-python-3-de

## Goals of this showcase

- Build a Flask based application with an embedded database
- Run tests
- Create an installable package
- Create a Docker image

### Implemented Goals

_Working on gitlab.com with a local gitlab runner attached (the used python modules are mentioned in parentheses)_

- Run code quality checks (flake8, pylint)
- SAST against source code and dependencies (bandit, jake)
- Run unittests, record coverage and test results (coverage, tox, pytest)
- Build a python package (build)
- Upload the python package to a registry (twine) → <https://gitlab.com/j2c-bce/showcase-python-flask/-/packages>
- Build a docker image using [Kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) and upload it to a registry → <https://gitlab.com/j2c-bce/showcase-python-flask/container_registry/>
- Create HTML documentation (mkdocs)
- _implicit:_ upload documentation to Gitlab Pages → <https://j2c-bce.gitlab.io/showcase-python-flask/>)

### Future Goals

- Integration tests
- DAST

## Onboarding

_Instructions how to run the application locally for testing purposes_

### Variant 1: Use the container image built by the pipeline

Go to the [Container registry](https://gitlab.com/j2c-bce/showcase-python-flask/container_registry/)
and look for the desired tag of the **flaskblog** container image.
Copy the tag to the clipboard and paste it to the end of the command

```
docker run -p 8080:8080
```

e.g. `docker run -p 8080:8080 registry.gitlab.com/j2c-bce/showcase-python-flask/flaskblog:SNAPSHOT-main`

Point your web browser to local port **8080** to access the application: <http://localhost:8080/>

### Variant 2: Use the package built by the pipeline

Set up and activate a virtual environment (see Variant 3 below).

Download the package from <https://gitlab.com/j2c-bce/showcase-python-flask/-/packages>
and store it locally, or follow the instructions on the detail page
how to fetch the package directly using pip.

Initialize the database using the `init_db` command.

Set the `FLASK_APP=flaskblog.core` environment variable, start the
application using `python -m flask run` and access the application
at local port **5000**: <http://localhost:5000/>.

### Variant 3: Run from source

#### Clone this repository and change into the directory

```
git clone https://gitlab.com/j2c-bce/showcase-python-flask.git
cd showcase-python-flask
```

(or download the zip file, unpack it and change into the directory)

#### Create a virtual environment

##### Unix/Linux

`python3 -m venv venv` or `virtualenv venv`

##### Windows

`python -m venv venv`

In restricted environments where you have no direct internet access,
the following works if the required packages (see below) are already installed
system-wide:

`python -m venv --system-site-packages venv`

#### Activate the virtual environment

##### Unix/linux

`source venv/bin/activate`

##### Windows

`venv\Scripts\activate`

#### Install the required packages

`pip install Flask python-dateutil`

#### Set environment variables

(on Unix/Linux using `export`, on Windows using `set`:

- `FLASK_APP=flaskblog.core`
- `FLASK_DEBUG=true`
- `PYTHONPATH=src`

#### Initialize the database

`python -c "from flaskblog import core; core.init_db()"`

#### Start the application

`python -m flask run`

Point your web browser to local port **5000** to access the application: <http://localhost:5000/>

## Further details

Please have a look at [Project layout](./docs/project-layout.md).
