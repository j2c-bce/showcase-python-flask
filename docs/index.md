# Flaskblog

This is a showcase project for a python application using [Flask](https://flask.palletsprojects.com/).

## The application

The application is a minimal blog-like web application with an embedded database.
You can create, read, update or delete posts.
If deployed using the docker image, the database will be reset to its initial state.

## Purpose of this project

This project is intended for experimentation with test tools and build systems.
