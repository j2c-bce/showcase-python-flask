# Project layout

## Overview

```
showcase-python-flask
├── Dockerfile
├── .dockerignore
├── docs
│   ├── index.md
│   └── project-layout.md
├── .gitignore
├── .gitlab-ci.yml
├── MANIFEST.in
├── mkdocs.yml
├── pyproject.toml
├── README.md
├── requirements.in
├── requirements.txt
├── setup.cfg
├── src
│   └── flaskblog
│       ├── backported.py
│       ├── core.py
│       ├── __init__.py
│       ├── model
│       │   └── schema.sql
│       ├── projectglobals.py
│       ├── static
│       │   ├── css
│       │   │   └── bootstrap.min.css
│       │   └── images
│       │       └── flask-logo.ico
│       └── templates
│           ├── base.html
│           ├── create.html
│           ├── index.html
│           ├── read.html
│           └── update.html
├── tests
│   ├── __init__.py
│   ├── test_backported.py
│   ├── test_core.py
│   └── test_projectglobals.py
├── tools
│   ├── extract_percentage.awk
│   ├── filter_pylint_rc.py
│   ├── make_image_version.py
│   ├── project_version.py
│   └── substitute_envvars.py
└── tox.ini
```

## Details

### The web application

Flaskblog is a web application using Flask, implementing the
[Large Applications as Packages](https://flask.palletsprojects.com/en/2.0.x/patterns/packages/)
pattern.

```
├── src
│   └── flaskblog
│       ├── backported.py
│       ├── core.py
│       ├── __init__.py
│       ├── model
│       │   └── schema.sql
│       ├── projectglobals.py
│       ├── static
│       │   ├── css
│       │   │   └── bootstrap.min.css
│       │   └── images
│       │       └── flask-logo.ico
│       └── templates
│           ├── base.html
│           ├── create.html
│           ├── index.html
│           ├── read.html
│           └── update.html
```

### Unit tests

These tests are run in the testcoverage and tox-pytest stages:

```
├── tests
│   ├── __init__.py
│   ├── test_backported.py
│   ├── test_core.py
│   └── test_projectglobals.py
```

For each application module, one test module has been created.

### CI pipeline

The pipeline and test configuration currently uses the official Python 3.6-slim image from Dockerhub.
Newer images require the adjustment of both the base image in **`.gitlab-ci.yml`**
and the environment in **`tox.ini`**.

**Note:** the pipeline requires the python packages marked in **bold**
with all their dependencies
_in addition to the packages listed in the **`requirements.txt`** file_.

Configuration:

```
├── .gitlab-ci.yml
```

See <https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html> for the official documentation.

Some of the pipeline jobs use helper scripts from the **`tools`** subdirectory:

```
├── tools
│   ├── extract_percentage.awk
│   ├── filter_pylint_rc.py
│   ├── make_image_version.py
│   ├── project_version.py
│   └── substitute_envvars.py
```

Pipeline jobs in order of definition:

#### check-version

Compares the project version defined in `src/flaskblog/projectglobals.py`
to the tag version if the pipeline was started from a Git tag.

#### flake8

Static source code check against [PEP 8](https://www.python.org/dev/peps/pep-0008/) using **flake8**

#### pylint

Static source code check using **pylint**

#### sast-source

Checks for possible security issues in the application source using **bandit**.

#### sast-dependencies

Checks for known security issues in the application’s dependencies using **jake**.

#### testcoverage

Unit tests, recordint test coverage using **coverage**

#### tox-pytest

Unit tests using **tox**.
This will temporarily create a virtual environment and also install **pytest** there.

```
└── tox.ini
```

#### build-package

Creates the python package (flaskblog-…-py3-none-any.whl) using **build**

Configuration:

```
├── MANIFEST.in
├── (…)
├── pyproject.toml
├── (…)
├── requirements.txt
├── setup.cfg
```

This job will create a subdirectory named **`dist`** that is used in the
`upload-package` and `kaniko-build` jobs:

```
├── dist
│   ├── flaskblog-…-py3-none-any.whl
│   └── flaskblog-….tar.gz
```

The build result is also available for download from the pipeline.

#### upload-package

Uploads the built package to a package index using **twine**

#### kaniko-build

Builds a container image using Kaniko and uploads it to a container registry

```
├── Dockerfile
├── .dockerignore
```

#### pages

Generates HTML documentation using **mkdocs**:

```
├── docs
│   ├── index.md
│   └── project-layout.md
│ (…)
├── mkdocs.yml
```

The documentation is built inside a new subdiretory called **`site`**,
in this case:

```
├── site
│   ├── 404.html
│   ├── css
│   │   ├── theme.css
│   │   └── theme_extra.css
│   ├── fonts
│   │   ├── fontawesome-webfont.eot
│   │   ├── fontawesome-webfont.svg
│   │   ├── fontawesome-webfont.ttf
│   │   ├── fontawesome-webfont.woff
│   │   ├── fontawesome-webfont.woff2
│   │   ├── Lato
│   │   │   ├── lato-bold.eot
│   │   │   ├── lato-bolditalic.eot
│   │   │   ├── lato-bolditalic.ttf
│   │   │   ├── lato-bolditalic.woff
│   │   │   ├── lato-bolditalic.woff2
│   │   │   ├── lato-bold.ttf
│   │   │   ├── lato-bold.woff
│   │   │   ├── lato-bold.woff2
│   │   │   ├── lato-italic.eot
│   │   │   ├── lato-italic.ttf
│   │   │   ├── lato-italic.woff
│   │   │   ├── lato-italic.woff2
│   │   │   ├── lato-regular.eot
│   │   │   ├── lato-regular.ttf
│   │   │   ├── lato-regular.woff
│   │   │   └── lato-regular.woff2
│   │   └── RobotoSlab
│   │       ├── roboto-slab.eot
│   │       ├── roboto-slab-v7-bold.eot
│   │       ├── roboto-slab-v7-bold.ttf
│   │       ├── roboto-slab-v7-bold.woff
│   │       ├── roboto-slab-v7-bold.woff2
│   │       ├── roboto-slab-v7-regular.eot
│   │       ├── roboto-slab-v7-regular.ttf
│   │       ├── roboto-slab-v7-regular.woff
│   │       └── roboto-slab-v7-regular.woff2
│   ├── img
│   │   └── favicon.ico
│   ├── index.html
│   ├── js
│   │   ├── jquery-2.1.1.min.js
│   │   ├── modernizr-2.8.3.min.js
│   │   ├── theme_extra.js
│   │   └── theme.js
│   ├── project-layout
│   │   └── index.html
│   ├── search
│   │   ├── lunr.js
│   │   ├── main.js
│   │   ├── search_index.json
│   │   └── worker.js
│   ├── search.html
│   ├── sitemap.xml
│   └── sitemap.xml.gz
```

That directory is renamed to **`public`** and passed to a follow-up job
auto-generated by the Gitlab CI, which in turn deploys it to
`$CI_PAGES_URL` (<https://j2c-bce.gitlab.io/showcase-python-flask>).

