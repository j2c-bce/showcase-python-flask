# -*- coding: utf-8 -*-

"""

core.py

Simple blogging application, core module

This file is part of flaskblog,
an example flask application based on the instructions from
<https://www.digitalocean.com/community/tutorials/
 how-to-make-a-web-application-using-flask-in-python-3-de>

"""

import datetime
import os
import sqlite3
import tempfile

from dateutil import tz
from flask import Flask, render_template, request, url_for, flash, redirect
from werkzeug.exceptions import abort

# Local backports module for Python 3.6,
# project global variables
from flaskblog import backported
from flaskblog import projectglobals


__version__ = projectglobals.__version__

LOCAL_TIME_ZONE = tz.gettz("Europe/Berlin")


def get_db_connection():
    """Return the database connection"""
    connection = sqlite3.connect(
        os.path.join(tempfile.gettempdir(), "database.db")
    )
    connection.row_factory = sqlite3.Row
    return connection


def get_post_data(post_id):
    """Return data from the post specified by post_id"""
    conn = get_db_connection()
    post_data = conn.execute(
        "SELECT * FROM posts WHERE post_id = ?", (post_id,)
    ).fetchone()
    conn.close()
    if post_data is None:
        abort(404)
    #
    return post_data


application = Flask(__name__)
application.config["SECRET_KEY"] = os.urandom(24).hex()


def init_db():
    """Initialize the database"""
    connection = get_db_connection()
    with open(
        os.path.join(application.root_path, "model", "schema.sql"),
        encoding="utf-8",
    ) as schema_file:
        connection.executescript(schema_file.read())
    #
    cur = connection.cursor()
    cur.execute(
        "INSERT INTO posts (title, content) VALUES (?, ?)",
        (
            "Willkommen beim Showcase Python/Flask!",
            "Anlässlich des heutigen BCE-Techtlk zeige ich euch,"
            " wie ich dieses Projekt über GitLab gebaut habe.",
        ),
    )
    connection.commit()
    connection.close()
    print("Initialized database schema.")


@application.template_filter("local_time_display")
def local_time_display(dt_string, local_time_zone=LOCAL_TIME_ZONE):
    """Return an UTC datetime converted to local time"""
    provided_dt = backported.datetime_fromisoformat(dt_string)
    if not provided_dt.tzinfo:
        provided_dt = provided_dt.replace(tzinfo=datetime.timezone.utc)
    #
    localized_dt = provided_dt.astimezone(local_time_zone)
    return f"{localized_dt} ({localized_dt.tzname()})"


@application.route("/")
def index():
    """Application home page showing all post headlines"""
    template_data = dict(version=__version__)
    conn = get_db_connection()
    template_data["posts"] = conn.execute(
        "SELECT * FROM posts ORDER BY created DESC"
    ).fetchall()
    conn.close()
    return render_template("index.html", **template_data)


@application.route("/<int:post_id>")
def show_post(post_id):
    """Detail page showing the post headline and content"""
    template_data = dict(
        version=__version__,
        post_data=get_post_data(post_id),
    )
    return render_template("read.html", **template_data)


@application.route("/create", methods=("GET", "POST"))
def create():
    """Page for creating new posts"""
    if request.method == "POST":
        title = request.form["title"]
        content = request.form["content"]
        if title:
            conn = get_db_connection()
            conn.execute(
                "INSERT INTO posts (title, content) VALUES (?, ?)",
                (title, content),
            )
            conn.commit()
            conn.close()
            return redirect(url_for("index"))
        #
        flash("Title is required!")
    #
    template_data = dict(version=__version__)
    return render_template("create.html", **template_data)


@application.route("/<int:post_id>/edit", methods=("GET", "POST"))
def edit(post_id):
    """Page for editing existing posts"""
    if request.method == "POST":
        title = request.form["title"]
        content = request.form["content"]
        if title:
            conn = get_db_connection()
            conn.execute(
                "UPDATE posts SET title = ?, content = ? WHERE post_id = ?",
                (title, content, post_id),
            )
            conn.commit()
            conn.close()
            return redirect(url_for("show_post", post_id=post_id))
        #
        flash("Title is required!")
    #
    template_data = dict(
        version=__version__,
        post_data=get_post_data(post_id),
    )
    return render_template("update.html", **template_data)


@application.route("/<int:post_id>/delete", methods=("POST",))
def delete(post_id):
    """URL for deleting existing posts"""
    post_data = get_post_data(post_id)
    conn = get_db_connection()
    conn.execute("DELETE FROM posts WHERE post_id = ?", (post_id,))
    conn.commit()
    conn.close()
    flash(f"{post_data['title']!r} was successfully deleted!")
    return redirect(url_for("index"))


#
