# -*- coding: utf-8 -*-

"""

projectglobals.py

Project global variables,
referenced in the mail module as well as the setup.cfg for the project

This file is part of flaskblog,
an example flask application based on the instructions from
<https://www.digitalocean.com/community/tutorials/
 how-to-make-a-web-application-using-flask-in-python-3-de>

"""


__version__ = "0.6.1"
