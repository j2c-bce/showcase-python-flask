# -*- coding: utf-8 -*-

"""

test the backported module

"""

import datetime
import unittest

from flaskblog import backported


class TestSimple(unittest.TestCase):

    """Test the backported module"""

    def test_fromisoformat_naive(self):
        """Check datetime parsing"""
        parsed_dt = backported.datetime_fromisoformat(
            "2021-10-22 15:33:24.321"
        )
        self.assertFalse(isinstance(parsed_dt.tzinfo, datetime.tzinfo))
        self.assertEqual(
            parsed_dt,
            datetime.datetime(2021, 10, 22, 15, 33, 24, 321000),
        )
        self.assertEqual(
            backported.datetime_fromisoformat("2021-10-22"),
            datetime.datetime(2021, 10, 22, 0, 0, 0, 0),
        )

    def test_fromisoformat_aware(self):
        """Check datetime parsing"""
        parsed_dt = backported.datetime_fromisoformat(
            "2021-10-22 15:33:24+02:00"
        )
        self.assertTrue(isinstance(parsed_dt.tzinfo, datetime.tzinfo))
        self.assertEqual(
            parsed_dt.tzinfo,
            datetime.timezone(datetime.timedelta(hours=2)),
        )
        self.assertEqual(
            parsed_dt.replace(tzinfo=None),
            datetime.datetime(2021, 10, 22, 15, 33, 24),
        )
        utc_dt = backported.datetime_fromisoformat(
            "2021-10-22 15:33:24+00:00"
        )
        self.assertTrue(isinstance(utc_dt.tzinfo, datetime.tzinfo))
        self.assertEqual(
            utc_dt.tzinfo,
            datetime.timezone.utc,
        )
        self.assertEqual(
            utc_dt.replace(tzinfo=None),
            datetime.datetime(2021, 10, 22, 15, 33, 24),
        )

    def test_invalid_argument(self):
        """Test various exceptions raised"""
        self.assertRaises(
            TypeError,
            backported.datetime_fromisoformat,
            2021,
            # fromisoformat: argument must be str"
        )

    def test_invalid_date_separator(self):
        """Test various exceptions raised"""
        self.assertRaises(
            ValueError,
            backported.datetime_fromisoformat,
            "2021/10-22 15:33:24",
            # "Invalid date separator"
        )
        self.assertRaises(
            ValueError,
            backported.datetime_fromisoformat,
            "2021-10/22 15:33:24",
            # "Invalid date separator"
        )

    def test_invalid_time_component(self):
        """Test various exceptions raised"""
        self.assertRaises(
            ValueError,
            backported.datetime_fromisoformat,
            "2021-10-22 1",
            # "Isoformat time too short"
        )
        self.assertRaises(
            ValueError,
            backported.datetime_fromisoformat,
            "2021-10-22 15:33:2",
            # "Incomplete time component"
        )
        self.assertRaises(
            ValueError,
            backported.datetime_fromisoformat,
            "2021-10-22 15/33/24",
            # "Invalid time separator: /"
        )
        self.assertRaises(
            ValueError,
            backported.datetime_fromisoformat,
            "2021-10-22 15:33:24,123",
            # "Invalid microsecond component" (wrong separator)
        )
        self.assertRaises(
            ValueError,
            backported.datetime_fromisoformat,
            "2021-10-22 15:33:24.1234",
            # "Invalid microsecond component" (wrong length)
        )

    def test_malformed_time_zone(self):
        """Test various exceptions raised"""
        self.assertRaises(
            ValueError,
            backported.datetime_fromisoformat,
            "2021-10-22 15:33:24+02:00:x",
            # "Malformed time zone string"
        )


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
