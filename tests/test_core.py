# -*- coding: utf-8 -*-

"""

test the core module

"""

import unittest

from flaskblog import core


class TestSimple(unittest.TestCase):

    """Test the core module"""

    def test_database(self):
        """Check database functions"""
        core.init_db()
        self.assertEqual(
            core.get_post_data(1)["title"],
            "Willkommen beim Showcase Python/Flask!",
        )


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
