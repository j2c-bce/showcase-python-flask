# -*- coding: utf-8 -*-

"""

test the projectglobals module

"""

import unittest

from flaskblog import projectglobals


class TestSimple(unittest.TestCase):

    """Test the core module"""

    def test_version(self):
        """Check if the version number has three parts"""
        version = projectglobals.__version__
        self.assertEqual(len(version.split(".")), 3)


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
