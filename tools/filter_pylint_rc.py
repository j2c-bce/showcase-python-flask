#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

filter_pylint_rc.py

Filter out message categories from the pylint return code
See <https://docs.pylint.org/en/1.6.0/run.html#exit-codes>

This file is part of showcase-python-flask,
a Gitlab CI showcase project incorporating flaskblog,
an example flask application compiled by me based on the instructions from
<https://www.digitalocean.com/community/tutorials/
 how-to-make-a-web-application-using-flask-in-python-3-de>

"""


import argparse
import logging
import sys


CATEGORY_BITMASKS = dict(C=16, R=8, W=4, E=2, F=1)


def __get_arguments():
    """Parse command line arguments"""
    argument_parser = argparse.ArgumentParser(
        description="Filter pylint returncode"
    )
    argument_parser.set_defaults(loglevel=logging.INFO)
    argument_parser.add_argument(
        "-v",
        "--verbose",
        action="store_const",
        const=logging.DEBUG,
        dest="loglevel",
        help="Output all messages including debug level",
    )
    argument_parser.add_argument(
        "pylint_returncode",
        type=int,
        help="Returncode from the pylint run",
    )
    cat_ids = ", ".join(CATEGORY_BITMASKS)
    argument_parser.add_argument(
        "categories",
        help=f"The categories to filter out (any combination of {cat_ids})",
    )
    return argument_parser.parse_args()


def main(arguments):
    """Main script function"""
    logging.basicConfig(
        format="%(levelname)-8s\u2551 %(message)s", level=arguments.loglevel
    )
    filter_bitmask = 0xff
    for message_category in set(arguments.categories):
        try:
            current_bitmask = CATEGORY_BITMASKS[message_category.upper()]
        except KeyError:
            logging.warning("Ignored invalid category %r", message_category)
            continue
        #
        logging.debug("Filtering out category %r", message_category)
        filter_bitmask = filter_bitmask ^ current_bitmask
    #
    return arguments.pylint_returncode & filter_bitmask


if __name__ == '__main__':
    sys.exit(main(__get_arguments()))


#
