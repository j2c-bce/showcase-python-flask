#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

make_image_version.py

Make an image version from the build branch and the timestamp
of the build job

This file is part of showcase-python-flask,
a Gitlab CI showcase project incorporating flaskblog,
an example flask application compiled by me based on the instructions from
<https://www.digitalocean.com/community/tutorials/
 how-to-make-a-web-application-using-flask-in-python-3-de>

"""


import os
import sys


if __name__ == '__main__':
    sys.path.append('src')
    from flaskblog import projectglobals
    PROJECT_VERSION = projectglobals.__version__
    CI_COMMIT_REF_NAME = os.environ["CI_COMMIT_REF_NAME"]
    try:
        CI_COMMIT_TAG = os.environ["CI_COMMIT_TAG"]
    except KeyError:
        print(f"SNAPSHOT-{CI_COMMIT_REF_NAME}")
        sys.exit(0)
    #
    if CI_COMMIT_TAG != f"v{PROJECT_VERSION}":
        raise ValueError("Commit tag does not match project version!")
    #
    if CI_COMMIT_TAG != CI_COMMIT_REF_NAME:
        raise ValueError("Commit tag does not match commit reference!")
    #
    print(PROJECT_VERSION)


#
