#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

project_version.py

Print the version of the project
as read from flaskblog.projectglobals

This file is part of showcase-python-flask,
a Gitlab CI showcase project incorporating flaskblog,
an example flask application compiled by me based on the instructions from
<https://www.digitalocean.com/community/tutorials/
 how-to-make-a-web-application-using-flask-in-python-3-de>

"""


from flaskblog import projectglobals


if __name__ == '__main__':
    print(projectglobals.__version__)


#
